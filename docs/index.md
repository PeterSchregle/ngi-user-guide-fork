![](images/n-vision.png) 
========================

##nGI Library User Guide

**Generic Image Processing and Image Analysis**

![](images/mandrill.png)

*Copyright (c) 2015 Impuls Imaging GmbH*  
*All rights reserved.* 

![](images/ImpulsLogo.png)

   
**Impuls Imaging GmbH**  
Schlingener Str. 4  
86842 Türkheim 

Germany/European Union

[http://www.impuls-imaging.com](http://www.impuls-imaging.com)

![](images/made-in-germany.png)

