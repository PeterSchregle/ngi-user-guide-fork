Digital cameras are important image input devices. Many digital color
cameras use a single image sensor in order to lower cost. These cameras
require the use of a CFA (color filter array) in front of the sensor
pixels to be able to acquire color. Each pixel thus samples the
intensity of just one color primary.

Here is an example of a typical Bayer Mosaic color arrangement, which
puts red, green and blue filters in front of the sensor pixels.

![The Bayer arrangement of color filters on the pixel array of an image
sensor (source: www.wikipedia.com, Colin M.L.
Burnett)](images/700px-Bayer_pattern_on_sensor.png)

In order to recover full color images from such a CFA based detector, a
method to calculate the missing colors at each pixel is needed, which is
commonly called a color demosaicing algorithm. Due to the popularity of
the Bayer pattern, the demosaicing functions currently work with the
Bayer pattern exclusively. A variety of algorithms are known and
implemented within NGI, but they all somehow have to make a trade-off
between quality and speed. Depending on the needs of the quality-speed
balance, the proper algorithm should be selected. Algorithms for
demosaicing can be classified into non-adaptive and adaptive algorithms.
Non-adaptive algorithms perform interpolation with a fixed pattern for
each pixel, while adaptive algorithms may use different interpolation
patterns depending on the image content. Thus, adaptive algorithms tend
to be more complex in general.

Nearest neighbour demosaicing is the simplest of the demosaicing
methods. Each interpolated value is taken from the nearest neighbour.

![Nearest neighbour demosaicing is done by pixel replication
independently in the color
channels.](images/demosaic_nearest_neighbor.png)

Bilinear demosaicing uses linear interpolation in two directions in
order to achieve better image quality.

Bilinear demosaicing of the red value on a green pixel is done by
averaging the horizontal neighbours: $R=\\frac{R\_1+R\_2}{2}$.

![Bilinear demosaicing of the red value on a green
pixel.](images/demosaic_bilinear_red_on_green_pixel.png)

Bilinear demosaicing of the blue value on a green pixel is done by
averaging the vertical neighbours: $B=\\frac{B\_1+B\_2}{2}$.

![Bilinear demosaicing of the blue value on a green
pixel.](images/demosaic_bilinear_blue_on_green_pixel.png)

Bilinear demosaicing of the red value on a blue pixel is done by
averaging the diagonal neighbours: $R=\\frac{R\_1+R\_2+R\_3+R\_4}{4}$.

![Bilinear demosaicing of the red value on a blue
pixel.](images/demosaic_bilinear_red_on_blue_pixel.png)

Bilinear demosaicing of the blue value on a red pixel is done by
averaging the diagonal neighbours: $B=\\frac{B\_1+B\_2+B\_3+B\_4}{4}$.

![Bilinear demosaicing of the blue value on a red
pixel.](images/demosaic_bilinear_blue_on_red_pixel.png)

Bilinear demosaicing of the green value on a red pixel is done by
averaging the horizontal or vertical neighbours, taking the correlation
in the red channel into account:

$$G=\\left\\{\\begin{array}{cl} \\frac{G\_1+G\_4}{2}, & \\mbox{if }\\vert R\_1-R\_4 \\vert \< \\vert R\_2-R\_3 \\vert \\\\ \\frac{G\_2+G\_3}{2}, & \\mbox{if }\\vert R\_1-R\_4 \\vert \> \\vert R\_2-R\_3 \\vert \\\\ \\frac{G\_1+G\_2+G\_3+G\_4}{4}, & \\mbox{if } \\vert R\_1-R\_4 \\vert = \\vert R\_2-R\_3 \\vert \\end{array}\\right.$$

![Bilinear demosaicing of the green value on a red
pixel.](images/demosaic_bilinear_green_on_red_pixel.png)

Bilinear demosaicing of the green value on a blue pixel is done by
averaging the horizontal or vertical neighbours, taking the correlation
in the blue channel into account:

$$G=\\left\\{\\begin{array}{cl} \\frac{G\_1+G\_4}{2}, & \\mbox{if }\\vert B\_1-B\_4 \\vert \< \\vert B\_2-B\_3 \\vert \\\\ \\frac{G\_2+G\_3}{2}, & \\mbox{if }\\vert B\_1-B\_4 \\vert \> \\vert B\_2-B\_3 \\vert \\\\ \\frac{G\_1+G\_2+G\_3+G\_4}{4}, & \\mbox{if } \\vert B\_1-B\_4 \\vert = \\vert B\_2-B\_3 \\vert \\end{array}\\right.$$

![Bilinear demosaicing of the green value on a blue
pixel.](images/demosaic_bilinear_green_on_blue_pixel.png)

Adaptive homogeneity demosaicing uses a sophisticated and recent
algorithm to reduce interpolation artefacts.

The various demosaicing methods implemented in nGI exhibit different
characteristics. Nearest neighbour demosaicing is the simplest algorithm
but it shows serious zippering and other artefacts. Bilinear
interpolation is better and reduces zippering considerably. The best
algorithm is adaptive homogeneity-directed demosaicing, but this is also
the slowest variant.

![From left to right: overview picture of nearest neighbour demosaicing,
bilinear demosaicing and adaptive homogeneity-directed demosaicing. Seen
from a distance they all look visually
similar.](images/bayer_demosaic_overview.png)

![From left to right: detail picture of nearest neighbor demosaicing,
bilinear demosaicing and adaptive homogeneity-directed demosaicing. The
AHD algorithm reduces many of the color and zippering
artefacts.](images/bayer_demosaic_detail.png)
