**nGI** supports decoding barcodes.

Several one-dimensional and two-dimensional symbologies are supported.
Here is an alphabetical list of the supported symbologies:

<table>
<colgroup>
<col width="8%" />
<col width="10%" />
<col width="81%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">symbology</th>
<th align="left">dimensionality</th>
<th align="left">link to description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Aztec</td>
<td align="left">2D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Aztec_Code"><a href="http://en.wikipedia.org/wiki/Aztec_Code" class="uri">http://en.wikipedia.org/wiki/Aztec_Code</a></a></td>
</tr>
<tr class="even">
<td align="left">Code39</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Code_39"><a href="http://en.wikipedia.org/wiki/Code_39" class="uri">http://en.wikipedia.org/wiki/Code_39</a></a></td>
</tr>
<tr class="odd">
<td align="left">Code93</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Code_93"><a href="http://en.wikipedia.org/wiki/Cod3_93" class="uri">http://en.wikipedia.org/wiki/Cod3_93</a></a></td>
</tr>
<tr class="even">
<td align="left">Code128</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Code_128"><a href="http://en.wikipedia.org/wiki/Code_128" class="uri">http://en.wikipedia.org/wiki/Code_128</a></a></td>
</tr>
<tr class="odd">
<td align="left">Data Matrix</td>
<td align="left">2D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Data_Matrix"><a href="http://en.wikipedia.org/wiki/Data_Matrix" class="uri">http://en.wikipedia.org/wiki/Data_Matrix</a></a></td>
</tr>
<tr class="even">
<td align="left">EAN</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/International_Article_Number_(EAN)"><a href="http://en.wikipedia.org/wiki/International_Article_Number_(EAN)" class="uri">http://en.wikipedia.org/wiki/International_Article_Number_(EAN)</a></a></td>
</tr>
<tr class="odd">
<td align="left">ITF</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/ITF-14"><a href="http://en.wikipedia.org/wiki/ITF-14" class="uri">http://en.wikipedia.org/wiki/ITF-14</a></a></td>
</tr>
<tr class="even">
<td align="left">PDF417</td>
<td align="left">1D stacked</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/PDF417"><a href="http://en.wikipedia.org/wiki/PDF417" class="uri">http://en.wikipedia.org/wiki/PDF417</a></a></td>
</tr>
<tr class="odd">
<td align="left">QR Code</td>
<td align="left">2D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/QR_code"><a href="http://en.wikipedia.org/wiki/QR_code" class="uri">http://en.wikipedia.org/wiki/QR_code</a></a></td>
</tr>
<tr class="even">
<td align="left">UPC</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Universal_Product_Code"><a href="http://en.wikipedia.org/wiki/Universal_Product_Code" class="uri">http://en.wikipedia.org/wiki/Universal_Product_Code</a></a></td>
</tr>
</tbody>
</table>

The decoder has a configuration parameter, which you can select the
symbologies that you want to decode. By default, all of the supported
symbologies are enabled.

![The symbology selection via the configuration object of the barcode
decoder.](images/symbology_configuration.png)

The decoder expects one code in the image that is given. It expects the
code to sit roughly in the middle and for one-dimensional codes the
scanning is horizontal. This means that the bars should be vertical. A
virtual horizontal scan line that sits in the middle should cross the
whole code.

Here is an example of a perfectly positioned code:

![Perfect position of code for the decoder.](images/Code39h.png)

The code in the following image cannot be decoded:

![The decoder cannot decode this image, because the bars are
horizontal.](images/Code39v.png)

The code in the following image does not decode, because the virtual
scan line does not cross the whole code:

![The decoder cannot decode this image, because the virtual scan line
does not cross the whole code.](images/Code3930.png)

The barcode decoder does not do anything else to locate a barcode. If
your circumstances are different, you can use other preprocessing tools,
to bring the barcode in a suitable position for the decoder. If the
barcode is not properly rotated, you can use the rotation tools. If the
barcode is not properly located, you can use the crop tool to cut out
the relevant portion for the barcode decoder. If contrast or brightness
are not sufficient, you can use image preprocessing.

Here is an example that shows how you would use the decoder:

    #include <ngi_image.h>
    #include <ngi_rgb.h>
    #include <ngi_locator.h>
    #include <ngi_view.h>
    #include <ngi_widget_image.h>
    #include <ngi_display.h>
    #include <ngi_iostream.h>
    #include <ngi_barcode_algorithm.h>
    #include <ngi_demo_main.h>

    using namespace ngi;

    typedef image<rgb<unsigned char> > image_type;
    typedef locator<image_type::value_type> locator_type;
    typedef view<locator_type> view_type;
    typedef widget_image<view_type> widget_type;
    typedef display<widget_type> display_type;

    int main(int argc, char* argv[])
    {
        image_type img = import_image<image_type>(TEXT("2dcode_suite\\datamatrix\\DataMatrix 10x10 012345.png"));
        display_type wnd(img.get_view(), TEXT("Barcode"));

        ngi::barcode_decoder configuration;

        auto barcodes = ngi::decode_barcode(img.get_view(), configuration);

        ngi::cout << barcodes.size() << TEXT(" barcode(s) found");
        ngi::cout << std::endl;
        for (auto bc = barcodes.begin(); bc != barcodes.end(); ++bc)
        {
            ngi::cout << TEXT("[ ") << bc->get_text() << TEXT(" ] ");
            auto position = bc->get_position();
            for (auto p = position.begin(); p != position.end(); ++p)
                ngi::cout << TEXT("(") << p->get_x() << TEXT(" ") << p->get_y() << TEXT(") ");
            ngi::cout << bc->get_symbology_name();
            ngi::cout << std::endl;
        }

        run_message_loop();

        return 0;
    }
