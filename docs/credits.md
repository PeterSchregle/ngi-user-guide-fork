**nGI** would not have been possible if we would not have had the
opportunity to build on numerous talented people’s work.

We are grateful to the work of these people and thank them for the
effort they have put in their work. By building upon these people’s
work, **nGI** is much more powerful than it would have been otherwise.

We have used tools, code and accumulated wisdom of other people, and in
this section we want to acknowledge their work and thank them.

Boost
=====

Boost (<http://www.boost.org>) is "one of the most highly regarded and
expertly designed C++ library projects in the world." These are not our
words, but those of Herb Sutter and Andrej Alexandrescu.

![The Boost Logo](images/The_Boost_Logo.png)

We have used lots of functionality from Boost, and by this the NGI
source code is much cleaner, shorter and better as it could have been
otherwise.

FreeImage
=========

FreeImage (<http://freeimage.sourceforge.net/>) is the library we use to
load and save images from and to various formats.

![The FreeImage Logo](images/The_FreeImage_Logo.png)

This software uses the FreeImage open source image library.

Cmake
=====

CMake (<http://www.cmake.org/>) is a family of tools designed to build,
test and package software.

![The Cmake Logo](images/The_Cmake_Logo.png)

We use CMake to build the NGI samples with different compilers and in
different configurations. We also use CMake to compile and run tests.

Subversion
==========

Subversion (<http://subversion.apache.org/>) is an open source version
control system.

![The Subversion Logo](images/The_Subversion_Logo.png)

We use Subversion to manage the development of NGI. Subversion is a set
of command line tools.

TortoiseSVN
===========

TortoiseSVN (<http://tortoisesvn.tigris.org/>) provides a user interface
to Subversion.

![The TortoiseSVN Logo](images/The_TortoiseSVN_Logo.png)

It comes in the form of a Windows explorer extension and is much easier
to use than the subversion command line.

Doxygen and Graphviz
====================

Doxygen (<http://www.doxygen.org>) is a documentation system for C++ and
other languages.

![The Doxygen Logo](images/The_Doxygen_Logo.png)

We use Doxygen to automatically build the NGI reference documentation
from the NGI source files.

Graphviz (<http://www.graphviz.org/>) is a set of tools for graph
visualization. It is optionally used by Doxygen to create inheritance
and collaboration graphs.

Wikipedia
=========

Wikipedia (<http://www.wikipedia.org/>) is an invaluable resource of
knowledge and we have often used it while creating NGI. We also took the
freedom to augment our documentation with links to Wikipedia.

![The Wikipedia Logo](images/The_Wikipedia_Logo.png)

Mathworld
=========

Mathworld (<http://mathworld.wolfram.com/>) is a piece of mathematical
beauty, created by Eric Weisstein and supported by Wolfram. I have often
scratched my head while trying to gain mathematical insight when finally
Mathworld came to our rescue. Where applicable, we have put links to
Mathworld from within the documentation.

![The Mathworld Logo](images/The_Mathworld_Logo.png)
