Matrices, and the rules to calculate with them, are a fundamental
mathematical technique, which is often used in image processing. As
such, a decent way to work with matrices had to be included within nGI.

Instead of starting from scratch, the matrix functions in NGI were
influenced heavily by JAMA, the Java Matrix Package. JAMA itself was
based on EISPACK, LAPACK, LINPACK and others, and uses the same
algorithms as those.

The matrix support in nGI is comprised of six classes: `matrix`,
`lu_decomposition`, `qr_decomposition`, `cholesky_composition`,
`singular_value_decomposition` and `eigenvalue_decomposition`.

The `matrix` class provides the fundamental operations of numerical
linear algebra. Various constructors create matrices of floating point
numbers. Various gets and sets provide access to submatrices and matrix
elements. The basic arithmetic operations include matrix addition and
multiplication, matrix norms and selected element-by-element array
operations.

Five fundamental matrix decompositions, which consist of pairs or
triples of matrices, permutation vectors, and the like, produce results
in five decomposition classes. These decompositions are accessed by the
`matrix` class to compute solutions of simultaneous linear equations,
determinants, inverses and other matrix functions.

The five decompositions are:

·         LU decomposition (Gaussian elimination) of rectangular
matrices.

·         QR decomposition of rectangular matrices.

·         Singular value decomposition of rectangular matrices.

·         Cholesky decomposition of symmetric, positive definite
matrices.

·         Eigenvalue decomposition of both symmetric and nonsymmetrical
square matrices.
