Erosion
=======

Erosion is a variant of one of the two fundamental operations in
morphological image processing.

At every pixel location in the image a binary structuring element is
used to probe the neighborhood. The result of the erosion at that pixel
is the minimum of all neighbors selected with the structuring element.

    // Erosion from a source view to a destination view.
    template<typename VS, typename VD, typename VK> 
    void erode(VS source, VD destination, VK kernel);

    // Erosion from a source view to a destination view, 
    // inside a region only.
    template<typename VS, typename VD, typename VK> 
    void erode(VS source, region const& aoi, VD destination, 
        VK kernel);

    // Erosion from a source view to a destination view.
    Morphology.Erode(View source, View dest, View kernel);

    // Erosion from a source image to a destination image.
    Image destination = source.Erode(View kernel);

    // Erosion from a source view to a destination view, 
    // inside a region only.
    Morphology.Erode(View source, Region aoi, View dest, View kernel);

    // Erosion from a source image to a destination image, 
    // inside a region only.
    Image destination = source.Erode(View source, Region aoi, 
        View kernel);

Here are some examples of the erosion:

![](images/erode_3x3.png)

Erosion with 3x3 kernel.

![](images/erode_5x5.png)

Erosion with 5x5 kernel.

![](images/erode_7x7.png)

Erosion with 7x7 kernel.

![](images/erode_9x9.png)

Erosion with 9x9 kernel.

Dilation
========

Dilation is a variant of one of the two fundamental operations in
morphological image processing.

At every pixel location in the image a binary structuring element is
used to probe the neighborhood. The result of the dilation at that pixel
is the maximum of all neighbors selected with the structuring element.

    // Erosion from a source view to a destination view.
    template<typename VS, typename VD, typename VK> 
    void dilate(VS source, VD destination, VK kernel);

    // Erosion from a source view to a destination view, 
    // inside a region only.
    template<typename VS, typename VD, typename VK> 
    void dilate(VS source, region const& aoi, VD destination, 
        VK kernel);

    // Dilation from a source view to a destination view.
    Morphology.Dilate(View source, View dest, View kernel);

    // Dilation from a source image to a destination image.
    Image destination = source.Dilate(View kernel);

    // Dilation from a source view to a destination view, 
    // inside a region only.
    Morphology.Dilate(View source, Region aoi, View dest, View kernel);

    // Dilation from a source image to a destination image, 
    // inside a region only.
    Image destination = source.Dilate(View source, Region aoi, 
        View kernel);

Here are some examples of the dilation:

![](images/dilate_3x3.png)

Dilation with 3x3 kernel.

![](images/dilate_5x5.png)

Dilation with 5x5 kernel.

![](images/dilate_7x7.png)

Dilation with 7x7 kernel.

![](images/dilate_9x9.png)

Dilation with 9x9 kernel.
