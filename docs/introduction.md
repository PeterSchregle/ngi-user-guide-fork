This text is written for programmers who want to write image processing
and image analysis code. **nGI** is a powerful image processing and
image analysis library with applications in fields such as **machine
vision**, **scientific imaging** and **life sciences**.

The core of **nGI** is written in C++ for a variety of reasons, where
one of the most important ones is performance. **nGI** also has a
flexible wrapper generation technique that allows it to be used from
managed code within .NET.

A user of **nGI** typically is a programmer fluent in C++ who needs to
add image processing and image analysis functions to his application.
While **nGI** is very advanced and extremely flexible we have still
tried to keep the learning curve smooth. **nGI** has different layers
that differ in ease of use and flexibility.

The first part of the book helps you getting started with **nGI** for
.NET and gives a broad overview. The second part is concerned with image
processing. The third part is concerned with image processing. There is
also an extensive reference documentation, which is generated from
function documentation in the source code.

**nGI** is commercially licensed. You can only use it if you have a
written agreement with Impuls Imaging GmbH. Any usage that is not
explicitly allowed under such an agreement is prohibited.

![](images/ngi_overview.png)

**nGI** is a generic image processing and image analysis library. Let us
dissect the important terms of this definition.

**Image processing** is the global name for programming techniques that
deal with images on input and recreate other images on output.

**Image analysis** goes a step further in that it tries to extract
meaning from an image. Usually the output of an image analysis algorithm
is not another image, but some different data, such as the number of
objects in an image.

**Library** is the term for a piece of code that’s available in the form
of a toolbox. Software pieces to be assembled by a programmer to fulfill
a task. The **nGI** library is a C++ library available in source code.

**Generic** is the opposite of specific. While traditionally libraries
have been available in specific forms, the programmer community is now
making the move to code in generic forms. This means that the algorithms
in a generic library are applicable to a wider range of problems,
because they are not so specific. One of the challenges of generic code
is to remain as performing as specific code. We will say more about this
in later chapters.

**nGI** is delivered complete with source code, documentation (you are
reading it now) and sample programs. The documentation should give you
an overview about **nGI**, its architecture and components, as well as
how to use and extend it.

The sample code is provided to give you a quick start in trying **nGI**.
The samples have been written by the designers of **nGI**, so they
usually show the intended way to use **nGI**. Feel free to reuse the
sample code and copy portions of it to your applications.

Finally the source code is provided to give you full control. You can
only gain full control with source code. You can read the code or single
step through the code with a debugger to gain understanding.

This document is designed to give you a full overview about **nGI**. It
is not a reference documentation, but it should be exactly what you need
when starting with **nGI** and trying to learn how to best use it. You
can approach **nGI** from different perspectives, and the documentation
kind of reflects these different perspectives and groups information
under a few broad topics.

The broadest perspective is the architecture of **nGI**. With this we
mean everything about **nGI** that has nothing to do with the code
itself, such as the directory layout or the testing and benchmarking
processes.

On another level there is the conceptual description of **nGI**. This
means the description of the concepts that have been followed within
**nGI**. Examples are the locator and view concepts.

Finally there is the functional perspective. The actual functionality
with respect to image processing and image analysis is described in
different chapters of this documentation. In addition to the
documentation in this text, extensive reference documentation is
available as well.

Getting Started with C++
========================

The very first thing you should do is to download the bits and build
**nGI**. See "How to Build **nGI**" for more information about this.

Then you are ready to explore the samples that are located in the
`./samples/console` directory. Most samples try to load images and they
expect them to be in the `./images` directory. The build process expands
the images there. Make sure that the `NGI_IMAGE_PATH` environment
variable points to this directory, so that the code can find the images.

All samples come with source code that you can read in order to
understand what is going on. Here is an example that should get you
started with coding:

    // This sample shows you how to load an image from a file 
    // and display it.

    #include <ngi_display.h>
    #include <ngi_widget_image.h>
    #include <ngi_buffer.h>
    #include <ngi_import_image.h>
    #include <ngi_locator.h>
    #include <ngi_view.h>
    #include <ngi_rgb.h>
    #include <ngi_demo_main.h>

    using namespace **nGI**;

    typedef buffer<rgb<unsigned char> > image_type;
    typedef locator<image_type::value_type> locator_type;
    typedef view<locator_type> view_type;
    typedef widget_image<view_type> widget_type;
    typedef display<widget_type> display_type;

    int main(int argc, char* argv[])
    {
        image_type img = import_image<image_type>(TEXT("fish.png"));
        display_type wnd(img.get_view(), TEXT("Fish"));

        run_message_loop();

        return 0;
    }

When run, this sample loads an image and displays it in a window. The
sample quits when you either press the space bar while the image window
has the focus, or close the image window with the close button. If this
code does not appear to work, please check that the file `fish.pgm`
could be found. The file `fish.pgm` is in the `./images` directory (but
only after the build process has expanded it), and this directory should
be referenced by the `NGI_IMAGE_PATH` environment variable.

![A screenshot of the file\_access.exe
program](./images/file_access_screenshot.png)

**nGI** is supplied with full source code, and it is heavily documented.
You can have a look at the files in the `./samples` and `./include`
directories in order to understand more about **nGI**.

Have fun!
