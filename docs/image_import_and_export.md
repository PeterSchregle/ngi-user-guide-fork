**nGI** itself does not have any direct facilities to load or save
images from files, streams or other locations. Instead, it relies on
other libraries to provide these facilities and simply interfaces to
them. Currently **nGI** uses the FreeImage library to load and store
images. Among the many formats that FreeImage supports, we consider the
following most important: **TIF**, **PNG**, **BMP**, **JPG** and
**GIF**.

Importing Images
================

**nGI** implements the `import_image` function, defined in the
`ngi_import_image.h` header, which is used to import images from a file.
This is how you use the function:

    buffer<unsigned char> image = import_image<buffer<unsigned char> >("lena.tif");

The function creates a buffer of a size that is suitable for the image
stored in the file, loads the data from the file into this buffer, and
finally returns the buffer to the caller.

Since `import_image` cannot deduce its template parameter, you need to
supply it with the call.

The data contained in the file and the data format you specify with the
buffer (also called its bit-depth) must correspond, otherwise the
function will fail. Not all file formats support all data types. If in
doubt, check on the FreeImage website or read the specification of the
file format in question.

Exporting Images
================

**nGI** implements the `export_image` function, defined in the
`ngi_export_image.h` header, which is used to export images from a file.
This is how you use the function:

    export_image(image, "test.tif");

The function detects the desired file format from the extension of the
passed string and then saves the image into the file.

The `export_image` function can deduce its template parameter, so it
does not need to be specified (although you certainly can in order to
make your intent more obvious).

The data format you specify with the buffer (also called its bit-depth)
and the desired file format must correspond, otherwise the function will
not work. Not all file formats support all data types. If in doubt,
check on the FreeImage website or read the specification of the file
format in question.
