The term moment stems from mathematics, where it describes quantitative
measures of the shape of a set of points. In physics, the moment can
refer to many different concepts, but all have in common that some
physical quantity is multiplied by a length to give a moment. A moment
used in image processing and related fields is some weighted average of
pixel intensities. Image moments are useful to describe objects. Some
simple features of images which are found via image moments include the
area, the accumulated intensity, the centroid of center of gravity and
other information about orientation.

![](images/moments_hierarchy.png)

Moments
=======

of order (*p* + *q*) are defined as:

*M*<sub>*p**q*</sub> = ∑<sub>*x*</sub>∑<sub>*y*</sub>*x*<sup>*p*</sup>*y*<sup>*q*</sup>*I*<sub>*x**y*</sub>

where *x* and *y* are the horizontal and vertical pixel coordinates and
*I*<sub>*x**y*</sub> is the pixel intensity at location (*x*, *y*).

If pixel intensities are not important, so called binary moments can
also be calculated using the following formula:

*M*<sub>*p**q*</sub> = ∑<sub>*x*</sub>∑<sub>*y*</sub>*x*<sup>*p*</sup>*y*<sup>*q*</sup>

Moments are not invariant with respect to translation, scaling and
rotation.

The `region` class in **nGI** has a moments property that calculates
moments up to order 3 and returns them in the `moments` class. You can
use the properties `m00`, `m10`, `m01`, `m20`, `m11`, `m02`, `m30`,
`m21`, `m12` and `m03` to read the respective moments.

Simple image properties derived via moments are the area for binary
images (`m00`) or the sum of grey levels for greyscale images (`m00`).

Normalized moments
==================

of order (*p* + *q*) are defined as:

$${N}\_{{pq}}={\\frac{{M}\_{{pq}}}{{M}\_{{00}}}}$$

Normalized moments are invariant with respect to scaling, but are not
invariant with respect to translation and rotation.

The `normalized_moments` class in **nGI** calculates normalized moments
up to order 3 when given a `moments` instance. You can use the
properties `n10`, `n01`, `n20`, `n11`, `n02`, `n30`, `n21`, `n12` and
`n03` to read the respective normalized moments. There are also static
methods `calculate_n10`, `calculate_n01`, `calculate_n20`,
`calculate_n11`, `calculate_n02`, `calculate_n30`, `calculate_n21`,
`calculate_n12` and `calculate_n03` that you can use to calculate the
respective moment only.

A simple image property derived via normalized moments is the centroid:

$$\\left( \\begin{matrix} x \\\\ y \\end{matrix} \\right) =\\left( \\begin{matrix} \\frac { { N }\_{ 10 } }{ { N }\_{ 00 } }  \\\\ \\frac { { N }\_{ 01 } }{ { N }\_{ 00 } }  \\end{matrix} \\right)$$

Central moments
===============

of order (*p* + *q*) are defined as:

$${ \\mu  }\_{ pq }=\\frac { 1 }{ { M }\_{ 00 } } \\sum \_{ x }^{  }{ \\sum \_{ y }^{  }{ { (x-\\overline { x } ) }^{ p }{ (y-\\overline { y } ) }^{ q }{ I }\_{ xy } }  }$$

If pixel intensities are not important, so called binary central moments
can also be calculated using the following formula:

$${ \\mu  }\_{ pq }=\\frac { 1 }{ { M }\_{ 00 } } \\sum \_{ x }^{  }{ \\sum \_{ y }^{  }{ { (x-\\overline { x } ) }^{ p }{ (y-\\overline { y } ) }^{ q } }  }$$

Central moments are invariant with respect to translation and scaling,
but not with rotation.

The `central_moments` class in **nGI** calculates central moments up to
order 3 when given a `moments` instance. You can use the properties
`mu20`, `mu11`, `mu02`, `mu30`, `mu21`, `mu12` and `mu03` to read the
respective central moments. There are also static methods
`calculate_mu20`, `calculate_mu11`, `calculate_mu02`, `calculate_mu30`,
`calculate_mu21`, `calculate_mu12` and `calculate_mu03` that you can use
to calculate the respective moment only.

Simple image properties derived via central moments are major and minor
axes as well as the rotation angle of the equivalent ellipse.

From the equivalent ellipses major and minor axes, the eccentricity can
be calculated. The eccentricity can have values ranging from 0 to 1. It
is zero for a perfectly round object and 1 for a line shaped object.

Scale invariant moments
=======================

of order (*p* + *q*) are defined as:

$${\\eta}\_{{pq}}={\\frac{{\\mu}\_{{pq}}}{{\\mu}\_{{00}}^{\\frac{p+q}{2}+1}}}$$

Scale invariant moments are invariant with respect to translation and
scaling, but are not invariant with respect to rotation.

The `scale_invariant_moments` class in **nGI** calculates scale
invariant moments up to order 3 when given a `central_moments` instance.
You can use the properties `eta20`, `eta11`, `eta02`, `eta30`, `eta21`,
`eta12` and `eta03` to read the respective scale invariant moments.
There are also static methods `calculate_eta20`, `calculate_eta11`,
`calculate_eta02`, `calculate_eta30`, `calculate_eta21`,
`calculate_eta12` and `calculate_eta03` that you can use to calculate
the respective moment only.

Hu’s moments
============

are defined as:

$$\\begin{aligned}
    {\\phi}\_{{1}} & = & {{\\eta}\_{{20}}+{\\eta}\_{{02}}} \\\\
    {\\phi}\_{{2}} & = & {({\\eta}\_{{20}}-{\\eta}\_{{02}})^2+4{\\eta}\_{{11}}^2} \\\\
    {\\phi}\_{{3}} & = & {({\\eta}\_{{30}}-3{\\eta}\_{{12}})^2+(3{\\eta}\_{{21}}-{\\eta}\_{{03}})^2} \\\\
    {\\phi}\_{{4}} & = & {({\\eta}\_{{30}}+{\\eta}\_{{12}})^2+({\\eta}\_{{21}}+{\\eta}\_{{03}})^2} \\\\
    {\\phi}\_{{5}} & = & {({\\eta}\_{{30}}-3{\\eta}\_{{12}})({\\eta}\_{{30}}+{\\eta}\_{{12}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-3({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{+(3{\\eta}\_{{21}}-{\\eta}\_{{03}})({\\eta}\_{{21}}+{\\eta}\_{{03}})\\left[3({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\\\
    {\\phi}\_{{6}} & = & {({\\eta}\_{{20}}-{\\eta}\_{{02}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{+4{\\eta}\_{{11}}({\\eta}\_{{30}}+{\\eta}\_{{12}})({\\eta}\_{{21}}+{\\eta}\_{{03}})} \\\\
    {\\phi}\_{{7}} & = & {(3{\\eta}\_{{21}}-3{\\eta}\_{{03}})({\\eta}\_{{30}}+{\\eta}\_{{12}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-3({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{-({\\eta}\_{{30}}-3{\\eta}\_{{12}})({\\eta}\_{{21}}+{\\eta}\_{{03}})\\left[3({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]}\\end{aligned}$$

Hu’s moments are invariant with respect to translation, scaling and
rotation.

The `hu_moments` class in **nGI** calculates Hu’s moments when given a
`scale_invariant_moments` instance. You can use the properties `phi1`,
`phi2`, `phi3`, `phi4`, `phi5`, `phi6` and `phi7` to read the respective
Hu moment. There are also static methods `calculate_phi1`,
`calculate_phi2`, `calculate_phi3`, `calculate_phi4`, `calculate_phi5`,
`calculate_phi6` and `calculate_phi7` that you can use to calculate the
respective moment only.

Flusser’s moments
=================

are defined as:

$$\\begin{aligned}
    {I}\_{{1}} & = & {\\frac{{\\mu}\_{{20}}{\\mu}\_{{02}}-{\\mu}\_{{11}}^2}{{\\mu}\_{{00}}^4}} \\\\
    {I}\_{{2}} & = & {\\frac{{\\mu}\_{{30}}^2{\\mu}\_{{03}}^2-6{\\mu}\_{{30}}{\\mu}\_{{21}}{\\mu}\_{{12}}{\\mu}\_{{03}}+4{\\mu}\_{{30}}{\\mu}\_{{12}}^3+4{\\mu}\_{{21}}^3{\\mu}\_{{03}}-3{\\mu}\_{{21}}^2{\\mu}\_{{12}}^2}{{\\mu}\_{{00}}^10}} \\\\
    {I}\_{{3}} & = & {\\frac{{\\mu}\_{{20}}({\\mu}\_{{21}}{\\mu}\_{{03}}-{\\mu}\_{{12}}^2)-{\\mu}\_{{11}}({\\mu}\_{{30}}{\\mu}\_{{03}}-{\\mu}\_{{21}}{\\mu}\_{{12}}))}{{\\mu}\_{{00}}^7}} \\nonumber \\\\
    & & {+\\frac{{\\mu}\_{{02}}({\\mu}\_{{30}}{\\mu}\_{{12}}-{\\mu}\_{{21}}^2)}{{\\mu}\_{{00}}^7}} \\\\
    {I}\_{{4}} & = & {\\frac{{\\mu}\_{{20}}^3{\\mu}\_{{03}}^2-6{\\mu}\_{{20}}^2{\\mu}\_{{11}}{\\mu}\_{{12}}({\\mu}\_{{03}}-6{\\mu}\_{{20}}^2{\\mu}\_{{02}}{\\mu}\_{{21}}){\\mu}\_{{03}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{9{\\mu}\_{{20}}^2{\\mu}\_{{02}}{\\mu}\_{{12}}^2+12{\\mu}\_{{20}}{\\mu}\_{{11}}^2{\\mu}\_{{21}}{\\mu}\_{{03}}+6{\\mu}\_{{20}}{\\mu}\_{{11}}{\\mu}\_{{02}}{\\mu}\_{{30}}{\\mu}\_{{03}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {-\\frac{18{\\mu}\_{{20}}{\\mu}\_{{11}}{\\mu}\_{{02}}{\\mu}\_{{21}}{\\mu}\_{{12}}+8{\\mu}\_{{11}}^3{\\mu}\_{{30}}{\\mu}\_{{03}}+6{\\mu}\_{{20}}{\\mu}\_{{02}}^2{\\mu}\_{{30}}{\\mu}\_{{12}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{9{\\mu}\_{{20}}{\\mu}\_{{02}}^2{\\mu}\_{{21}}^2+12{\\mu}\_{{11}}^2{\\mu}\_{{02}}{\\mu}\_{{30}}{\\mu}\_{{12}}-6{\\mu}\_{{11}}{\\mu}\_{{02}}^2{\\mu}\_{{30}}{\\mu}\_{{21}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{{\\mu}\_{{02}}^3{\\mu}\_{{30}}^2}{{\\mu}\_{{00}}^{11}}}\\end{aligned}$$

Flusser moments are invariant with respect to affine transformations.

The `flusser_moments` class in nGI calculates flusser moments when given
a `central_moments` instance. You can use the properties `i1`, `i2`,
`i3`, and `i4` to read the respective flusser moments. There are also
static methods `calculate_i1`, `calculate_i2`, `calculate_i3` and
`calculate_i4` that you can use to calculate the respective moment only.
