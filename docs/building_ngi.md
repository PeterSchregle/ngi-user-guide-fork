Quick Start
===========

This chapter explains how to quickly start with **nGI** from scratch.
Follow this minimal step by step procedure to build **nGI**.

1.  Get **nGI** from Impuls Imaging GmbH.
2.  Download and build Boost.
3.  Download and build FreeImage.
4.  Run CMake.
5.  Build **nGI** from within Visual Studio.

The following chapters explain these steps in more details.

More Build Details
==================

Downloading **nGI**
-------------------

The **nGI** source code is delivered by Impuls Imaging GmbH via various
means, i.e. via DVD or electronic delivery. Your contact should have
provided you with some means to get the **nGI** source code.

Installing Third-Party Tools
----------------------------

**nGI** needs a few tools from third parties in order to be built and
used.

If you want to build **nGI** and run the sample programs and the test
programs, you need the CMake build tool. Download it from the
<http://www.cmake.org> website and install it on your machine.

If you want to build the reference documentation, you need Doxygen.
Download it from the <http://www.doxygen.org> website and install it on
your machine. If Doxygen is not installed, you cannot build the
reference documentation. Doxygen parses the source code and uses its
structure and special comments to produce reference documentation. Also
optional is GraphViz, which is used from within Doxygen to produce
inheritance and collaboration graphs (download it from
<http://www.graphviz.org>).

### Download and build Boost

In all cases, you need to download and build the Boost library.

I am doing this now on a machine with Windows 7 installed. I have the
Windows 7 SDK installed as well as Visual Studio 2012 Professional. The
same or a similar procedure should work if you are using a different
version of Windows, whether you have the Windows SDK installed or not
and with a different version of Visual Studio. Eventually you may need
to tweak the settings a little bit if your specific configuration is
different.

There is a very nice tutorial explaining how to begin with Boost at
[http://www.boost.org/more/getting\\\_started/index.html](http://www.boost.org/more/getting\_started/index.html).
Make sure to read this tutorial, since it has much more information than
this text.

#### Download Boost

Download Boost 1.53.0 (or later) from
<http://www.boost.org/users/download/>. If you follow the link, you are
forwarded to a page on sourceforge.net, where you can choose between
different files.

You will also need additional components later, when you will be
building Boost. Zlib can be downloaded at
<http://www.zlib.net/zlib127.zip>. Bzip2 can be downloaded at
<http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz>.

#### Install Boost

Install Boost by expanding the downloaded file.

I chose to download `boost_1_53.zip` and expand it to `C:/thirdparty`.

I also expanded Bzip2 and Zlib into the `C:/thirdparty` directory, i.e.
into C:/thirdparty/bzip2-1.0.6 and C:/thirdparty/zlib-1.2.7.

#### Build Boost

**nGI** needs some libraries that need to be separately built.
Currently, the following libraries are needed: `date_time`,
`filesystem`, `iostreams`, `serialization`, `test`, `thread`, `chrono`
and `timer`.

Boost can be built both for 32 and 64 bit targets. It depends on your
needs whether you need one or both versions.

To build Boost for 32 bits, open a 32 bit command shell with the correct
build environment by selecting the Start Menu - Programs - Microsoft
Visual Studio 2010 - Visual Studio Tools - Visual Studio 2010 Command
Prompt menu entry. Make sure that you right-click the menu entry and run
it as an administrator.

Change into the Boost folder of your installation (usually you would
type `cd /thirdparty/boost`).

To build Boost with the required options, type the following:

    bootstrap
    .\bjam --toolset=msvc-11.0 --with-date_time --with-filesystem 
    --with-iostreams -sBZIP2_INCLUDE=c:\thirdparty\bzip2-1.0.6 
    -sBZIP2_SOURCE=c:\thirdparty\bzip2-1.0.6 
    -sZLIB_INCLUDE=c:\thirdparty\zlib-1.2.7 
    -sZLIB_SOURCE=c:\thirdparty\zlib-1.2.7 
    --with-serialization --with-test --with-thread --with-chrono
    --with-timer --build-type=complete --stagedir=x86

This will build Boost 32 bit libraries as needed by **nGI** and put them
into the `c:/thirdparty/boost/x86/lib` directory.

To build Boost for 64 bits, open a 64 bit command shell with the correct
build environment by selecting the Start Menu - Programs - Microsoft
Visual Studio 2010 - Visual Studio Tools - Visual Studio 2010 x64 Cross
Tools Command Prompt menu entry. Make sure that you right-click the menu
entry and run it as an administrator.

Change into the Boost folder of your installation (usually you would
type `cd /thirdparty/boost`).

To build Boost with the required options, type the following:

    bootstrap
    .\bjam --toolset=msvc-11.0 address-model=64 --with-date_time 
    --with-filesystem --with-iostreams 
    -sBZIP2_INCLUDE=c:\thirdparty\bzip2-1.0.6 
    -sBZIP2_SOURCE=c:\thirdparty\bzip2-1.0.6 
    -sZLIB_INCLUDE=c:\thirdparty\zlib-1.2.7 
    -sZLIB_SOURCE=c:\thirdparty\zlib-1.2.7 --with-serialization 
    --with-test --with-thread --with-chrono --with-timer 
    --build-type=complete --stagedir=x64

This will build Boost 64 bit libraries as needed by NGI and put them
into the `c:/thirdparty/boost/x64/lib directory`.

### Download and build FreeImage

In most cases, when you want to load images from disk or save images to
disk, you need the FreeImage library.

I am doing this now on a machine with Windows 7 installed. I have the
Windows 7 SDK installed as well as Visual Studio 2012 Professional. The
same or a similar procedure should work if you are using a different
version of Windows, whether you have the Windows SDK installed or not
and with a different version of Visual Studio. Eventually you may need
to tweak the settings a little bit if your specific configuration is
different.

#### Download FreeImage

Download FreeImage 3.15.4 (or later) from
downloads.sourceforge.net/freeimage/FreeImage3154.zip. If you click the
link, the download of FreeImage3154.zip will start shortly.

#### Expand FreeImage

I chose to expand it to `C:/thirdparty/FreeImage`.

#### Convert the FreeImage solution

FreeImage comes with solutions for Visual Studio 2008 only, but you can
use the automatic conversion of Visual Studio 2012. Start Visual Studio
2012 and open `C:/thirdparty/FreeImage/FreeImage/FreeImage.2008.sln`.
Visual Studio 2012 will convert the solution and all projects in the
solution.

#### Build the FreeImage solution

Make sure that the FreeImage project is selected, switch to the Release
configuration and use the Build command from the Visual Studio 2012
Build menu.

This will build FreeImage and put the binaries into the
`C:/thirdparty/FreeImage/FreeImage/Dist` directory.

Running CMake
-------------

Before **nGI** can be built, you need to use CMake to create the build
environment, such as the Visual Studio solution and project files. Start
CMake and point it to the directory where you have copied the **nGI**
source files. It will find the CMakeLists.txt file and present the
following dialog.

CMake will look for Boost and FreeImage in certain preset locations, as
well as in locations pointed to by environment variables. Use
`NGI_BOOST_ENV` to point to the location where you have built boost
(i.e. `C:/thirdparty/boost`) and use `NGI_FREEIMAGE_ENV` to point to the
location where you have built FreeImage (i.e.
`C:/thirdparty/FreeImage`).

![The CMake User Interface](images/The_CMake_User_Interface.png)

You can see that CMake was able to find Boost, Cairo, Doxygen,
FreeImage, OpenGL and Qt - amoung others. Some of these components are
mandatory, others are optional. The fact that these entries are
displayed in red is CMake’s way to tell us that it needs some more
information. Click the Configure button - eventually more often than
once - in order to make the red colored entries go away.

Once all is well, the Generate button will be enabled, and when you
press Generate, CMake will create all the solution and project files
needed to build with Visual Studio. However, you can also change the
default settings to affect the way **nGI** is built. If you drill down
and expand the settings listed under the name NGI, you can see the
options and their current settings.

![Choosing Build Options](images/Choosing_Build_Options.png)

The table explains the options:

<table>
<colgroup>
<col width="20%" />
<col width="79%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">option</th>
<th align="left">description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><code>NGI_BUILD_DOCUMENTATION</code></td>
<td align="left">Builds the reference documentation from the source files. This option is on by default, when Doxygen is found. It is not available, when Doxygen was not found.</td>
</tr>
<tr class="even">
<td align="left"><code>NGI_ACCELERATE_AMP</code></td>
<td align="left">This is currently experimental and enables acceleration of certain functions using AMP. This is not to be used in production code yet.</td>
</tr>
<tr class="odd">
<td align="left"><code>NGI_ACCELERATE_IPP</code></td>
<td align="left">This option is set if Intel IPP (Intel Performance Primitives) is found on the system.</td>
</tr>
<tr class="even">
<td align="left"><code>NGI_ACCELERATE_TBB</code></td>
<td align="left">This option is set if Intel TBB (Intel Threading Building Blocks) is found on the system.</td>
</tr>
<tr class="odd">
<td align="left"><code>NGI_BUILD_DOCUMENTATION</code></td>
<td align="left">When this option is on, the reference documentation will be built using Doxygen.</td>
</tr>
<tr class="even">
<td align="left"><code>NGI_BUILD_FREEIMAGE_AS_STATIC_LIB</code></td>
<td align="left">This option is currently experimental and should be off.</td>
</tr>
<tr class="odd">
<td align="left"><code>NGI_BUILD_SAMPLES</code></td>
<td align="left">When this option is on, the samples will be built. When the option is off, the samples will not be built. The samples can be built for various targets which are mutually exclusive:</td>
</tr>
<tr class="even">
<td align="left"><code>NGI_BUILD_TESTS</code></td>
<td align="left">Builds and runs the tests.</td>
</tr>
<tr class="odd">
<td align="left"><code>NGI_BUILD_WITH_MVIMPACT_ACQUIRE_SUPPORT</code></td>
<td align="left">The Matrix Vision mvImpact Acquire interface will be used.</td>
</tr>
<tr class="even">
<td align="left"><code>NGI_BUILD_WITH_UEYE_SUPPORT</code></td>
<td align="left">The IDS uEye library will be used for camera interfacing.</td>
</tr>
<tr class="odd">
<td align="left"><code>NGI_BUILD_WRAPPERS</code></td>
<td align="left">The .NET wrapper library will be built.</td>
</tr>
<tr class="even">
<td align="left"><code>NGI_GRAPHICS_DIRECT2D</code></td>
<td align="left">The Direct2D interface will be used for graphics.</td>
</tr>
<tr class="odd">
<td align="left"><code>NGI_GRAPHICS_GDIPLUS</code></td>
<td align="left">The GDI+ interface will be used for graphics.</td>
</tr>
<tr class="even">
<td align="left"><code>NGI_GRAPHICS_OPENGL</code></td>
<td align="left">The OpenGL interface will be used for graphics.</td>
</tr>
</tbody>
</table>
