Tweening comes from the english word in between, and has been shortened
from in-betweening. It provides various methods in the form of functors
that calculate values in between 0 and 1 according to various functions.
Tweening can be used for purposes in the vicinity of interpolation, such
as the calculation of gradients in images and or palettes. It provides
linear and non-linear functions.

The following chapters do not show all functions. The square and square
root variants are mere examples of a more general class of tweening
functions, where the power can be chosen arbitrarily. The tweening
sample visualizes some more tweening functions.

Exact definitions of the formulas can be found in the function reference
documentation.

linear
======

![](images/tweening_linear.png)

binary
======

![](images/tweening_binary.png)

sinusoidal 1
============

![](images/tweening_sinusoidal_1.png)

sinusoidal 2
============

![](images/tweening_sinusoidal_2.png)

sinusoidal 3
============

![](images/tweening_sinusoidal_3.png)

sinusoidal 4
============

![](images/tweening_sinusoidal_4.png)

square 1
========

![](images/tweening_power_of_2_1.png)

square 2
========

![](images/tweening_power_of_2_2.png)

square 3
========

![](images/tweening_power_of_2_3.png)

square 4
========

![](images/tweening_power_of_2_4.png)

square root 1
=============

![](images/tweening_power_of_0_5_1.png)

square root 2
=============

![](images/tweening_power_of_0_5_2.png)

square root 3
=============

![](images/tweening_power_of_0_5_3.png)

square root 4
=============

![](images/tweening_power_of_0_5_4.png)
