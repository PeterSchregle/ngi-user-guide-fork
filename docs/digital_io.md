**nGI** has support for digital IO modules. This is helpful to control
machinery from image processing applications or to react upon input
signals sent from machinery.

Advantech Adam-60xx Ethernet Modules
====================================

**nGI** supports various digital IO modules from Advatech
([<http:www.advantech.com>](http://www.advantech.com)).

![](images/ADAM-6050_S.jpg)

The modules are connected via LAN or wireless LAN to a PC and support a
variety of input/output lines. The modules may have additional features,
such as counters, etc. but these are not supported.

<table>
<colgroup>
<col width="20%" />
<col width="12%" />
<col width="56%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">module</th>
<th align="left">bus</th>
<th align="left">features</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">ADAM-6050</td>
<td align="left">LAN</td>
<td align="left">12 digital inputs, 6 digital outputs</td>
</tr>
<tr class="even">
<td align="left">ADAM-6050W</td>
<td align="left">WLAN</td>
<td align="left">12 digital inputs, 6 digital outputs</td>
</tr>
<tr class="odd">
<td align="left">ADAM-6051</td>
<td align="left">LAN</td>
<td align="left">12 digital inputs, 2 digital outputs</td>
</tr>
<tr class="even">
<td align="left">ADAM-6051W</td>
<td align="left">WLAN</td>
<td align="left">12 digital inputs, 2 digital outputs</td>
</tr>
<tr class="odd">
<td align="left">ADAM-6052</td>
<td align="left">LAN</td>
<td align="left">8 digital inputs, 8 digital outputs</td>
</tr>
<tr class="even">
<td align="left">ADAM-6060</td>
<td align="left">LAN</td>
<td align="left">6 digital inputs, 6 digital outputs</td>
</tr>
<tr class="odd">
<td align="left">ADAM-6060W</td>
<td align="left">WLAN</td>
<td align="left">6 digital inputs, 6 digital outputs</td>
</tr>
<tr class="even">
<td align="left">ADAM-6066</td>
<td align="left">LAN</td>
<td align="left">6 digital outputs</td>
</tr>
</tbody>
</table>

Support for the Advantech Adam-60xx modules is implemented in the C\#
version of **nGI** only. The following class is available:

<table>
<colgroup>
<col width="10%" />
<col width="89%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">C# / .NET</th>
<th align="left">purpose</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><code>Adam6000Module</code></td>
<td align="left">An Advantech Adam-6000 module abstraction, whichis needed to intialize the module and get information from it. It is also needed to write to digital outputs and to read from digital inputs.</td>
</tr>
</tbody>
</table>

Data Translation OpenLayers
===========================

**nGI** supports various digital IO modules from Data Translation
([<http:www.datatranslation.com>](http://www.datatranslation.com)).

![](images/dt9817.jpg)

The modules are connected via USB or PCI buses to a PC and support a
variety of input/output lines. The modules may have additional features,
such as counters, etc. but these are not supported.

<table>
<colgroup>
<col width="18%" />
<col width="11%" />
<col width="56%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">module</th>
<th align="left">bus</th>
<th align="left">features</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">DT9817</td>
<td align="left">USB</td>
<td align="left">28 digital inputs/outputs</td>
</tr>
<tr class="even">
<td align="left">DT9817-H</td>
<td align="left">USB</td>
<td align="left">28 digital inputs/outputs</td>
</tr>
<tr class="odd">
<td align="left">DT9817-R</td>
<td align="left">USB</td>
<td align="left">8 digital inputs, 8 digital outputs</td>
</tr>
<tr class="even">
<td align="left">DT9835</td>
<td align="left">USB</td>
<td align="left">64 digital inputs/outputs</td>
</tr>
<tr class="odd">
<td align="left">DT335</td>
<td align="left">PCI</td>
<td align="left">32 digital inputs/outputs</td>
</tr>
<tr class="even">
<td align="left">DT351</td>
<td align="left">PCI</td>
<td align="left">8 digital inputs, 8 digital outputs</td>
</tr>
<tr class="odd">
<td align="left">DT340</td>
<td align="left">PCI</td>
<td align="left">32 digital inputs, 8 digital outputs</td>
</tr>
</tbody>
</table>

You need to install the Data Translation OpenLayers software in order to
use the modules from within **nGI**. We have used OpenLayers 7.5 to
implement the support, so OpenLayers 7.5 or higher is needed.

The following classes are available:

<table>
<colgroup>
<col width="11%" />
<col width="11%" />
<col width="76%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">C# / .NET</th>
<th align="left">C++ / native</th>
<th align="left">purpose</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><code>DtOpenlayersSystem</code></td>
<td align="left"><code>dt_openlayers_system</code></td>
<td align="left">Provide information about the installed DT OpenLayers software and the connected devices.</td>
</tr>
<tr class="even">
<td align="left"><code>DtOpenlayersModule</code></td>
<td align="left"><code>dt_openlayers_module</code></td>
<td align="left">A DT OpenLayers module abstraction, whichis needed to intialize the module and get information from it. It is also needed to write to digital outputs and to read from digital inputs.</td>
</tr>
</tbody>
</table>
