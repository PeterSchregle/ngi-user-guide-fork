There are quite a few different pixe types used within **nGI**. This
chapter explains most of them. The simplest type is monochrome and
capable to describe colorless shades of gray, ranging from darkest black
to brightest white. There is also an additive color type built from red,
green and blue primaries, named rgb. To both the monochrome and the rgb
type, **nGI** adds opacity or alpha, the opposite of transparency. This
gives the ability to describe gradually transparent colors. In addition
to these four basic color types, there exists a set of color spaces
which all have their own color types.

Color types are usually multi-dimensional. For example, an rgb type can
be considered as a vector with red, green and blue components. Besides
the rgb type, **nGI** provides a few other types that one could classify
among the color types. Grayscale and color can be combined with
transparency or alpha.

![The four basic color types.](images/color.png)

Color types can be converted into other types. The arrows specify
automatic or implicit conversions. Other conversions are narrowing (i.e.
information is lost) and need to be explicit.

![Conversions of the four basic color types.](images/basic_colors.png)

The color types combine with the arithmetic types and are used in a lot
of other types, such as buffers (images, histograms, palettes, profiles,
etc.), locators and views.

The arithmetic types consist of various integer types and some floating
point types. The integer types are either 8 bit, 16 bit or 32 bit wide,
and come in unsigned and signed forms.

An 8 bit unsigned pixel can hold values between 0 and 255
(2<sup>8</sup> − 1). By convention, 0 is assumed to correspond to the
darkest possible black while 255 is assumed to correspond to the
brightest possible white. Here is a memory layout of an 8 bit unsigned
pixel.

![8 bit integer type.](images/integer_8bit.png)

A 16 bit unsigned pixel can hold values between 0 and 65535
(2<sup>16</sup> − 1). By convention, 0 is assumed to correspond to the
darkest possible black while 65535 is assumed to correspond to the
brightest possible white. However, since displays are not capable of
displaying such a wide brightness range, usually window scaling or
leveling techniques are used to display a subset or this range only. If
the pixel is 16 bit signed it can hold values between -32768
( − 2<sup>15</sup>) and 32767 (2<sup>15</sup> − 1).

![16 bit integer type.](images/integer_16bit.png)

A 32 bit unsigned pixel can hold values between 0 and 4294967295
(2<sup>32</sup> − 1). By convention, 0 is assumed to correspond to the
darkest possible black while 4294967295 is assumed to correspond to the
brightest possible white. However, since displays are not capable of
displaying such a wide brightness range, usually window scaling or
leveling techniques are used to display a subset or this range only. If
the pixel is 32 bit signed it can hold values between -2147483648
( − 2<sup>31</sup>) and 2147483647 (2<sup>31</sup> − 1).

![32 bit integer type.](images/integer_32bit.png)

Here is a little sketch that shows the available integer types. Follow
the arrows to see how you can convert between integer types without a
loss of information. Any conversion that is not following the arrows
will lose information.

![Integer type conversions.](images/integer_conversions.png)

Two floating point types are commonly used: 32 bit and 64 bit. With both
types 0.0 is assumed to correspond to the darkest possible black while
1.0 is assumed to correspond to the brightest possible white. Values
below 0.0 are considered darker than the darkest black, and values above
1.0 are considered brighter than the darkest white. This may seem like a
contradiction, but it actually works. A 32 bit float type contains, from
right to left, a 23 bit mantissa, an 8 bit exponent and a 1 bit sign. A
64 bit float type contains, from right to left, a 53 bit mantissa, a 10
bit exponent and a 1 bit sign.

![32 bit integer type.](images/float.png)
