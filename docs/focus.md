Usually it is very important to acquire pictures which are in focus for
subsequent image processing or image analysis.

There are a few algorithms that can sharpen or deblur pictures, but they
are not perfect and all of them have side-effects that degrade some
aspects of the image. It is therefore best to make sure that the
pictures are in focus when they are acquired for the first place. NGI
provides a few focus functions that can be used to implement autofocus
algorithms based on image content.

This chapter explains the focus algorithms and also evaluates them based
on a few criteria:

1.The focus function should have only one maximum.

2.This maximum must be present when the system is focused.

3.The focus measurement should be reproducible.

4.The range of the focus function should be wide.

5.The focus function should be applicable to a wide range of image
types.

6.The focus function should be insensitive to other parameters such as
brightness.

A few of the thoughts in this chapter have been inspired by the paper “A
Comparison of Different Focus Functions for Use in Autofocus Algorithms”
by Groen, Young and Ligthart.

TODO

When you view a picture that is out of focus, and then bring it
gradually into focus, you can recognize a few observations:

1.The spread of gray tones increase as you get nearer to the focus
point.

2.The frequency of details in the picture gets higher as you get nearer
to the focus point.

TODO

These observations are the core of the two different focus algorithms
implemented.
